const mongoose = require('mongoose');

const FinanceSchema = mongoose.Schema(
    {
        desc: { type: String, required: true },
        nominal: { type: Number, required: true },
        // tanggal: { type: String, required: true }
        tanggal: { type: Date, default: Date.now }
    },
    {
        timestamps: true
    }
);


module.exports = mongoose.model('Finance', FinanceSchema);