const finance = require('../controller/finance.controller.js');

const route = (app) => {
    app.get('/getList', finance.findAll);
    app.get('/getSum', finance.getSum);
    app.post('/add', finance.saveData);
    app.put('/update/:id', finance.updateData);
    app.delete('/delete/:id', finance.deleteData);
};

module.exports = { route };