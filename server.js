const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const mongoose = require('mongoose');

app.use(bodyParser.json());


const url = 'mongodb://arisuu:pass1234@cluster0-shard-00-01-djrpz.mongodb.net:27017,cluster0-shard-00-02-djrpz.mongodb.net:27017,cluster0-shard-00-00-djrpz.mongodb.net:27017/main?ssl=true&replicaSet=Main-shard-0&authSource=admin&retryWrites=true';
// const url = 'mongodb+srv://arisuu:pass1234@cluster0-djrpz.mongodb.net/finance?retryWrites=true&w=majority';
mongoose.Promise = global.Promise;
const connect = async () => {
    try {
        await mongoose.connect(url, {
            useNewUrlParser: true,
            useUnifiedTopology: true
        });
        console.log('Connected to db. . .');

    } catch (err) {
        console.log('Could not connect to db : ', err);
        process.exit();
    }
}

connect();

const routes = require('./routes/finance.routes');

routes.route(app);

app.listen(3000, (req, res) => {
    console.log('Server is runing. . .');
})