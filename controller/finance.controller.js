const Redis = require('redis');
const FinanceMdl = require('../model/finance.model');

const findAll = async (req, res) => {
    try {
        const rds = Redis.createClient();
        rds.get('financeRecord', async (err, rst) => {
            if (err) throw err;
            if (rst) {
                res.json({ res: JSON.parse(rst), from: 'REDIS' });
            } else {
                try {
                    const financeRecord = await FinanceMdl.find({});

                    // set data into redis with 30s expired
                    rds.set('financeRecord', JSON.stringify(financeRecord), 'EX', 30, () => {
                        res.json({ res: financeRecord, from: 'MONGO' });
                    });
                } catch (err) {
                    console.log('Error : ', err);
                }
            }
        })
    } catch (err) {
        res.status(500).json({
            message: 'Error :' + err
        });
    }
}

const saveData = async (req, res) => {
    try {
        const newTrx = new FinanceMdl({
            desc: req.body.desc,
            nominal: req.body.nominal,
            tanggal: new Date(req.body.tanggal)
        })

        await newTrx.save();
        res.json({
            message: 'Success'
        });

    } catch (err) {
        res.status(500).json({
            message: 'Error :' + err
        });
    }
}


const updateData = async (req, res) => {
    try {

        let strWhere = { _id: req.params.id };

        const updData = {
            desc: req.body.desc,
            nominal: req.body.nominal,
            tanggal: new Date(req.body.tanggal)
        }


        const rst = await FinanceMdl.updateOne(strWhere, { $set: updData });

        res.json({ rst });
    } catch (err) {
        res.status(500).json({
            message: 'Error :' + err
        });
    }
}

const deleteData = async (req, res) => {
    try {
        let strWhere = { _id: req.params.id };

        const rst = await FinanceMdl.deleteOne(strWhere);

        res.json({ rst });
    } catch (err) {
        res.status(500).json({
            message: 'Error :' + err
        });
    }
}


const getSum = async (req, res) => {
    try {
        const rds = Redis.createClient();
        rds.get('financeRecordSum', async (err, rst) => {
            if (err) throw err;
            if (rst) {
                res.json({ res: JSON.parse(rst), from: 'REDIS' });
            } else {
                try {
                    const financeRecordSum = await FinanceMdl.aggregate([
                        {

                            $group: {
                                _id: { month: "$tanggal" },
                                total_Pengeluaran_Bulanan: { $sum: "$nominal" },
                            }
                        }]);


                    // set data into redis with 30s expired
                    rds.set('financeRecordSum', JSON.stringify(financeRecordSum), 'EX', 30, () => {
                        res.json({ res: financeRecordSum, from: 'MONGO' });
                    });
                } catch (err) {
                    console.log('Error : ', err);
                }
            }
        })
    } catch (err) {
        res.status(500).json({
            message: 'Error :' + err
        });
    }
}


module.exports = { findAll, saveData, deleteData, updateData, getSum };


